function countDown(time, discountTimer) {

    if (time > 0) {
        updateFirstTime(time);
        window.setTimeout(function () {
            countDown(time-1, discountTimer);
        }, 1000);
    } else {
        offerDiscount(discountTimer);
    }
}

function countDownNoButton (time) {
    if (time > 0) {
        window.setTimeout(function () {
            countDownNoButton(time-1);
        }, 1000);
    } else {
        showButton();
    }
}

function showButton () {
    $('#capture, .cta').slideToggle();
}

function countDownNoEmail (time, discountTimer) {
    if (time > 0) {
        window.setTimeout(function () {
            countDownNoEmail(time-1, discountTimer);
        }, 1000);
    } else {
        offerDiscountNoEmail(discountTimer);
    }
}

function updateFirstTime(time){
    time = secondsToTime(time);

    $('#ftimer-min').text(time.minutes + 'min');
    $('#ftimer-sec').text(time.seconds + 'sec');
}

function offerDiscountNoEmail (time) {
    timer(time);

    $('#timeout').removeClass('hide');
}

function offerDiscount (time) {
    timer(time);

    $('#timeout').removeClass('hide');
    $('#capture, .cta').slideToggle();
    $('#first-timer').slideToggle();
}

function timer (secs) {

    time = secondsToTime(secs);

    $('#timer-min').text(time.minutes);
    $('#timer-sec').text(time.seconds);

    if (secs > 0) {
        window.setTimeout(function () {
            timer(secs-1);
        }, 1000);
    }
}

function pad (n) {
    return (String(n).length < 2) ? '0' + n : n;

}

function secondsToTime (time) {
    var hours = Math.floor(time / 3600);
    var minutes = Math.floor((time - (hours * 3600)) / 60);
    var seconds = time - (hours * 3600) - (minutes * 60);
    return { hours: pad(hours), minutes: pad(minutes), seconds: pad(seconds) };
}